#include <iostream>
#include <cstdlib>
#include <ctime>
#include <iomanip>

using namespace std;

void table(int grid[][10]);
void elves(int grid[][10], int level);
int santa(int grid[][10]);
int elves_w(int grid[][10]);
int win(int grid[][10]);

//MAIN FUNCTION
int main()
{
	int grid[10][10] = {};
	int level = 1;
	srand(time(0));

	cout << setw(30) << endl << " WELLCOME TO THE SAVING SANTA FROM THE COVID " << endl;
	cout << setw(26) << endl << " --DETAIL-- " << endl << endl;
	cout << " [ S ] = SANTA " << endl;
	cout << " [ R ] = RUDOLPH " << endl;
	cout << " [ E ] = ELVES " << endl;
	cout << " [ C ] = COVID " << endl;
	cout << setw(30) << endl << " --HOW TO PLAY-- " << endl << endl;
	cout << " [ q ] = OBLIQUE TOP LEFT " << endl;
	cout << " [ w ] = UP " << endl;
	cout << " [ e ] = OBLIQUE TOP RIGHT " << endl;
	cout << " [ a ] = LEFT " << endl;
	cout << " [ s ] = DOWN " << endl;
	cout << " [ d ] = RIGHT " << endl;
	cout << " [ z ] = OBLIQUE DOWN LEFT " << endl;
	cout << " [ c ] = OBLIQUE DOWN RIGHT " << endl;
	cout << endl;

	while (true) {
		elves(grid, level);
		while (true) {
			table(grid);
			if (santa(grid) == 0) {
				cout << setw(30) << " YOU LOSE " << endl << endl;
				break;
			}
			if (elves_w(grid) == 0) {
				cout << setw(30) << " YOU LOSE " << endl << endl;
				break;
			}
			if (win(grid) == 0) {
				table(grid);
				cout << setw(30) << " YOU WIN " << endl << endl;
				level++;
				break;
			}
		}
	}
}

//TABLE
void table(int grid[][10]) {
	int count = 0;
	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 10; j++) {
			if (grid[i][j] == 0) {
				cout << "|   |";
			}
			else if (grid[i][j] == 1) {
				cout << "| S |";
			}
			else if (grid[i][j] == 2) {
				cout << "| R |";
			}
			else if (grid[i][j] == 3) {
				cout << "| E |";
			}
		}
		cout << endl;
		cout << "--------------------------------------------------\n";
	}
}
//ELVES 
void elves(int grid[][10], int level) {
	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 10; j++) {
			grid[i][j] = 0;
		}
	}
	grid[0][0] = 1;
	grid[9][9] = 2;
	int elves_num = 3 * level;
	for (int x = 0; x < (elves_num); x++) {
		int a = rand() % 10;
		int b = rand() % 10;
		if (a == 9 && b == 9) {
			x--;
			continue;
		}
		else if (a == 0 && b == 0) {
			x--;
			continue;
		}
		else {
			grid[a][b] = 3;
		}
	}
}

//ELVES WALK
int elves_w(int grid[][10]) {
	int random;
	int row, column;

	// random elves 
	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 10; j++) {
			if (grid[i][j] == 3) {
				random = rand() % 8;
				//i:row j:column
				row = i;
				column = j;
				if (random == 0) {
					if (row - 1 < 0 || column - 1 < 0) {
						j--;
						continue;
					}
					if (row == 9 && column == 9) {
						j--;
						continue;
					}
					if (grid[row - 1][column - 1] == 1) {
						return 0;
					}
					grid[row][column] = 0;
					grid[row - 1][column - 1] = 4;

				}
				else if (random == 1) {
					if (row - 1 < 0) {
						j--;
						continue;
					}
					if (row - 1 == 9 && column == 9) {
						j--;
						continue;
					}
					if (grid[row - 1][column] == 1) {
						return 0;
					}
					grid[row][column] = 0;
					grid[row - 1][column] = 4;
				}
				else if (random == 2) {
					if (row - 1 < 0 || column + 1 > 9) {
						j--;
						continue;
					}
					if (row - 1 == 9 && column + 1 == 9) {
						j--;
						continue;
					}
					if (grid[row - 1][column + 1] == 1) {
						return 0;
					}
					grid[row][column] = 0;
					grid[row - 1][column + 1] = 4;
				}
				else if (random == 3) {
					if (column - 1 < 0) {
						j--;
						continue;
					}
					if (row == 9 && column - 1 == 9) {
						j--;
						continue;
					}if (grid[row][column - 1] == 1) {
						return 0;
					}
					grid[row][column] = 0;
					grid[row][column - 1] = 4;
				}
				else if (random == 4) {
					if (column + 1 > 9) {
						j--;
						continue;
					}
					if (row == 9 && column + 1 == 9) {
						j--;
						continue;
					}
					if (grid[row][column + 1] == 1) {
						return 0;
					}
					grid[row][column] = 0;
					grid[row][column + 1] = 4;
				}
				else if (random == 5) {
					if (row + 1 > 9 || column - 1 < 0) {
						j--;
						continue;
					}
					if (row + 1 == 9 && column - 1 == 9) {
						j--;
						continue;
					}
					if (grid[row + 1][column - 1] == 1) {
						return 0;
					}
					grid[row][column] = 0;
					grid[row + 1][column - 1] = 4;
				}
				//lower mid
				else if (random == 6) {
					if (row + 1 > 9) {
						j--;
						continue;
					}
					if (row + 1 == 9 && column == 9) {
						j--;
						continue;
					}
					if (grid[row + 1][column] == 1) {
						return 0;
					}
					grid[row][column] = 0;
					grid[row + 1][column] = 4;
				}
				else if (random == 7) {
					if (row + 1 > 9 || column + 1 > 9) {
						j--;
						continue;
					}
					if (row + 1 == 9 && column + 1 == 9) {
						j--;
						continue;
					}if (grid[row + 1][column + 1] == 1) {
						return 0;
					}
					grid[row][column] = 0;
					grid[row + 1][column + 1] = 4;
				}
			}
		}
	}
	// convert grid[i][j] = 4 is grid[i][j] = 3
	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 10; j++) {
			if (grid[i][j] == 4) {
				grid[i][j] = 3;
			}
		}
	}
}

//SANTA
int santa(int grid[][10]) {
	int i, j;
	int r, c;
	char walk;
	for (i = 0; i < 10; i++) {
		for (j = 0; j < 10; j++) {
			if (grid[i][j] == 1) {
				//row = i and column = j
				r = i;
				c = j;
			}
		}
	}
	while (true) {
		cout << endl;
		cout << " input walk post : ";
		cin >> walk;
		cout << endl;
		if (walk == 'd') {
			if (c + 1 > 9) {
				cout << " CAN NOT MOVE " << endl << endl;
				continue;
			}
			if (grid[r][c + 1] == 3) {
				return 0;
			}
			if (grid[r][c] == 1) {
				grid[r][c + 1] = 1;
				grid[r][c] = 0;
			}
		}
		else if (walk == 'a') {
			if (c - 1 < 0) {
				cout << setw(30) << " CAN NOT MOVE " << endl << endl;
				continue;
			}
			if (grid[r][c] == 1) {
				grid[r][c - 1] = 1;
				grid[r][c] = 0;
			}
		}
		else if (walk == 'w') {
			if (r - 1 < 0) {
				cout << setw(30) << " CAN NOT MOVE " << endl << endl;
				continue;
			}
			if (grid[r - 1][c] == 3) {
				return 0;
			}
			if (grid[r][c] == 1) {
				grid[r - 1][c] = 1;
				grid[r][c] = 0;
			}
		}
		else if (walk == 's') {
			if (r + 1 > 9) {
				cout << setw(30) << " CAN NOT MOVE " << endl << endl;
				continue;
			}
			if (grid[r + 1][c] == 3) {
				return 0;
			}
			if (grid[r][c] == 1) {
				grid[r + 1][c] = 1;
				grid[r][c] = 0;
			}
		}
		else if (walk == 'q') {
			if (r - 1 < 0 && c - 1 < 0) {
				cout << setw(30) << " CAN NOT MOVE " << endl << endl;
				continue;
			}
			if (grid[r - 1][c - 1] == 3) {
				return 0;
			}
			if (grid[r][c] == 1) {
				grid[r - 1][c - 1] = 1;
				grid[r][c] = 0;
			}
		}
		else if (walk == 'e') {
			if (r - 1 < 0 && c + 1 > 9) {
				cout << setw(30) << " CAN NOT MOVE " << endl << endl;
				continue;
			}
			if (grid[r - 1][c + 1] == 3) {
				return 0;
			}
			if (grid[r][c] == 1) {
				grid[r - 1][c + 1] = 1;
				grid[r][c] = 0;
			}
		}
		else if (walk == 'z') {
			if (r + 1 > 9 && c - 1 < 0) {
				cout << setw(30) << " CAN NOT MOVE " << endl << endl;
				continue;
			}
			if (grid[r + 1][c - 1] == 3) {
				return 0;
			}
			if (grid[r][c] == 1) {
				grid[r + 1][c - 1] = 1;
				grid[r][c] = 0;
			}
		}
		else if (walk == 'c') {
			if (r + 1 > 9 && c + 1 > 9) {
				cout << setw(30) << " CAN NOT MOVE " << endl << endl;
				continue;
			}
			if (grid[r + 1][c + 1] == 3) {
				return 0;
			}
			if (grid[r][c] == 1) {
				grid[r + 1][c + 1] = 1;
				grid[r][c] = 0;
			}
		}
		break;
	}
}

//WIN
int  win(int grid[][10]) {
	if (grid[9][9] == 1) {
		return 0;
	}
}

